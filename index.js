/*
Criar um programa que calcula a média das turmas
de students e envia mensagem do calculo da média.
*/

const classA = [
    {
        name: 'Cleiton',
        grade: 9.8,
    },
    {
        name: 'Joao',
        grade: 5.1
    },
    {
        name: 'Juliano',
        grade: 7.9
    },
    {
        name: 'UM student',
        grade: 1
    }
]

const classB = [
    {
        name: 'Vinicius',
        grade: 1.9
    },
    {
        name: 'Gustavo',
        grade: 9.7
    },
    {
        name: 'Leonardo',
        grade: 2.7
    },
    {
        name: 'Novo student',
        grade: 5.0
    }
]

function calculateAverage(students) {
    let sum = 0;
    for (let i = 0; i < students.length; i++) {
        sum += students[i].grade;
    }
    const average = sum / students.length;
    return average;
}

const average1 = calculateAverage(classA);
const average2 = calculateAverage(classB);

function sendMessage(average, turma) {
    if (average > 5) {
        console.log(` ${turma} average: ${average}. Congrats!`)
    } else {
        console.log(`${turma} average: ${average}. Is not good!`)
    }
}

sendMessage(average1, 'Class A');
sendMessage(average2, 'Class B');

function markAsFlunked(student) {
    student.flunked = false;
    if (student.grade < 5) {
        student.flunked = true;
    }
}

function sendMessageflunked(student) {
    if (student.flunked) {
        console.log(`${student.name} flunked!`);
    }
}

function studentflunked(students) {
    for (let student of students) {
        markAsFlunked(student);
        sendMessageflunked(student);
    }
}

studentflunked(classA);
studentflunked(classB);
